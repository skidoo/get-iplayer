documentation & support:
The get_iplayer mailinglist Archives:  http://lists.infradead.org/pipermail/get_iplayer/
https://github.com/get-iplayer/get_iplayer/wiki
https://squarepenguin.co.uk/forums/


get_iplayer Known Issues            https://github.com/get-iplayer/get_iplayer/wiki/issues
    v3.22: Typo in option spec generates error with Perl 5.16
		https://github.com/get-iplayer/get_iplayer/pull/352
    v3.22: PVR is downloading TV programmes for Proms when only radio programmes specified
		https://forums.squarepenguin.co.uk/showthread.php?tid=2090
(skidoo also mentions these in the packaged htmldoc)

sources visited by skidoo:
https://launchpad.net/~jon-hedgerows/+archive/get-iplayer
https://bugs.launchpad.net/ubuntu/+source/get-iplayer
https://github.com/jon-hedgerows/get_iplayer_debian_packaging
https://github.com/get-iplayer/get_iplayer/
http://www.infradead.org/get_iplayer/html/get_iplayer.html
https://sources.debian.org/src/get-iplayer/2.78-2/
removed from debian7 (2015) https://www.debian.org/News/2015/2015090502
https://sourceforge.net/projects/get-iplayer/
.
https://advisories.mageia.org/MGAA-2017-0119.html (fix applied for v3.07)
gentoo RFP  (2017)  https://bugs.gentoo.org/296040
.
https://repology.org/project/get-iplayer/versions
AUR v3.22, homebrew & linuxbrew  v3.22, PCLinuxOS v3.22, SlackBuilds v3.22
.
https://www.redpacketsecurity.com/bbc-sniffing-wi-fi-packets-detect-bbc-iplayer-users-without-tv-licence/


license: GNU General Public License v3.0
